//
//  ViewController.swift
//  onboarding
//
//  Created by Ihwan ID on 30/08/20.
//  Copyright © 2020 Ihwan ID. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    private let scrollView = UIScrollView()
    private let pageControl: UIPageControl = {
        let pageControl = UIPageControl()
        pageControl.numberOfPages = 3
        pageControl.currentPageIndicatorTintColor = .brown
        pageControl.pageIndicatorTintColor = .blue
        //pageControl.backgroundColor = .systemBlue
        return pageControl
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        pageControl.addTarget(self, action: #selector(pageControlDidChange(_:)), for: .valueChanged)
        //scrollView.backgroundColor = .red
        scrollView.delegate = self
        view.addSubview(scrollView)
        view.addSubview(pageControl)


    }

    @objc private func pageControlDidChange(_ sender: UIPageControl){
        let current = sender.currentPage
        scrollView.setContentOffset(CGPoint(x: CGFloat(current) * view.frame.size.width, y: 0), animated: true)
    }

    override func viewDidLayoutSubviews() {
        pageControl.frame = CGRect(x: 10, y: view.frame.size.height - 100, width: view.frame.size.width - 20, height: 70)
        scrollView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height - 100)
        if(scrollView.subviews.count == 2){
            configureScrollView()
        }
    }

    func configureScrollView(){
        scrollView.contentSize = CGSize(width: scrollView.frame.size.width * 3, height: scrollView.frame.size.height)
        scrollView.isPagingEnabled = true
        for x in 0..<3{
            let page = UIView(frame: CGRect(x: CGFloat(x) * view.frame.size.width, y: 0, width: scrollView.frame.size.width, height: scrollView.frame.size.height))
            let text = UILabel(frame: CGRect(x: 100, y: 100, width: 200, height: 200))
            text.text = "hello 123"
            page.backgroundColor = .brown
            scrollView.addSubview(text)
            scrollView.addSubview(page)
        }
    }


}

extension ViewController: UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(floorf(Float(scrollView.contentOffset.x) / Float(scrollView.frame.size.width)))
    }
}

